const express = require('express');
const app = express();
const PORT = 8000;

let id;
let nome;
let raca;
let idade;
let situacao;

app.use(
    express.urlencoded({extended:true})
);


app.post('/pet', (req, res)=>{
    id = req.query.id;
    raca = req.query.raca;
    nome = req.query.nome
    idade = req.query.idade;
    situacao = req.query.situacao;

    res.send("<strong>Pet Cadastrado!</strong> <br>" +
            "<strong> Informações: </strong> <br>" +
            "<strong> Id: </strong>" + id + "<br>" +
            "<strong> Nome: </strong>" + nome + "<br>" +
            "<strong> Raça: </strong>" + raca + "<br>" +
            "<strong> Idade: </strong>" + idade + "<br>" +
            "<strong> Situação: </strong>" + situacao + "<br>");
})

app.get('/pet', (req,res)=>{
    if(req.query.nome === nome){
        res.status(200).send("<strong> Pet encontrado! </strong><br>" +
                             "<strong> Nome: </strong>" + nome + "<br>" +
                             "<strong> Raça: </strong>" + raca + "<br>" +
                             "<strong> Idade: </strong>" + idade + "<br>" +
                             "<strong> Situação: </strong>" + situacao + "<br>");
    }else{
        res.status(404).send("<strong> Pet NÃO encontrado! <strong>")
    }
});

app.put('/pet', (req,res)=>{
    if(req.query.id === id){
        res.status(200);
        if(situacao === "ativo"){
            situacao = "inativo"

            res.send("Situação Atualizada com Sucesso: <br>" +
            "<strong> Nome: </strong>" + nome + "<br>" +
            "<strong> Raça: </strong>" + raca + "<br>" +
            "<strong> Idade: </strong>" + idade + "<br>" +
            "<strong> Situação: </strong>" + situacao + "<br>");

        }else if(situacao === "inativo"){
            situacao = "ativo"

            res.send("Situação Atualizada com Sucesso: <br>" +
            "<strong> Nome: </strong>" + nome + "<br>" +
            "<strong> Raça: </strong>" + raca + "<br>" +
            "<strong> Idade: </strong>" + idade + "<br>" +
            "<strong> Situação: </strong>" + situacao + "<br>");
            
        }
    }else{
        res.status(400).send("<strong> Pet Não encontrado! </strong>")
    }
})

app.put('/petatual', (req,res)=>{
    if(req.query.id === id){
        res.status(200);

        raca = req.query.raca;
        nome = req.query.nome
        idade = req.query.idade;

        res.send("<strong>Pet Atualizado!</strong> <br>" +
            "<strong> Informações: </strong> <br>" +
            "<strong> Id: </strong>" + id + "<br>" +
            "<strong> Nome: </strong>" + nome + "<br>" +
            "<strong> Raça: </strong>" + raca + "<br>" +
            "<strong> Idade: </strong>" + idade + "<br>" +
            "<strong> Situação: </strong>" + situacao + "<br>");
    }else{
        res.status(400).send("<strong> ID incorreto! Pet Não encontrado! </strong>")
    }

})


app.listen(PORT,()=>{
    console.log(`PROJETO INICIADO NA PORTA ${PORT}`); 
});